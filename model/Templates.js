const connection = require('../db/connection');

const templates = {
    findTemplate:
    (username) => new Promise((resolve, reject) => {
        connection.query(`SELECT * FROM User_Templates 
        INNER JOIN Users ON users_id = Users.id 
        INNER JOIN Templates ON template_id = Templates.id 
        WHERE Users.username = ${username}`, (err, result) => {
            if (err) {
                reject(err);
            }
                resolve(result);
        });
    }),
    getAll:
    () => new Promise((resolve, reject) => {
        connection.query(`SELECT *  From Templates`, (err, result) => {
            if (err) {
                reject(err);
            }
                resolve(result);
        });
    }),
    getQuestionsById:
    (id)=> new Promise((resolve, reject) => {
        connection.query(`SELECT * FROM template_builder 
        LEFT JOIN Templates ON Templates.id = template_id 
        LEFT JOIN questions ON questions.id = question_id 
        WHERE Templates.id = ${id};`, (err, result) => {
            if(err) {
                reject(err);
            }
            resolve(result);
        });
    }),
    getTemplateById:
    (id) => new Promise((resolve, reject) => {
        connection.query(`SELECT * FROM Templates WHERE id = ${id};`, (err, result) => {
            if(err){
                reject(err);
            }
            resolve(result);
        });
    }),
    getQuestionsByTemplateId:
    (id) => new Promise((resolve, reject) => {
        connection.query(`SELECT * FROM template_builder 
        INNER JOIN Templates ON Templates.id = template_id
        INNER JOIN questions ON questions.id = question_id
        WHERE template_id = ${id};`, (err, result) => {
            if(err){
                reject(err);
            }
            resolve(result);
        });
    }),
    updateTemplate:
    (template) => new Promise((resolve, reject) => {
        connection.query(`UPDATE Templates
        SET name = IF((${template.name} IS NOT NULL OR ${template.name} != name),${template.name}, name),
        location = IF((${template.location} IS NOT null OR ${template.location} != location),${template.location}, location),
        classification = IF((${template.classification} IS NOT null OR ${template.classification} != classification),${template.classification}, classification),
        type = IF((${template.type} IS NOT null OR ${template.type} != type),${template.type}, type)
        WHERE id = ${template.id};`, (err, result) => {
            if(err){
                reject(err);
            }
            resolve(result);
        });
    }),
    addTemplateQuestions:
    (template_id, questionsArray) => new Promise((resolve, reject) => {
        let query = `START TRANSACTION;
        `
        let values = []
        questionsArray.forEach(element => {
            values.push(element.question !== undefined ? element.question : null)
            values.push(element.checkbox !== undefined ? element.checkbox : null)
            values.push(element.category !== undefined ? element.category : null)
            values.push(element.phase !== undefined ? element.phase : null)
            values.push(element.initiators !== undefined ? element.initiators : null)
            values.push(element.comments !== undefined ? element.comments : null)
            values.push(template_id)
            query += `INSERT INTO questions (question, checkbox, category, phase, initiators,comments) VALUES (?,?,?,?,?,?);
            INSERT INTO template_builder (template_id, question_id) VALUES (?, LAST_INSERT_ID());
            `
        });
        query += `COMMIT;`
        connection.query(query,values, (err, result) => {
            if(err){
                reject(err);
            }
            resolve(result);
        });
    })
}

module.exports = templates