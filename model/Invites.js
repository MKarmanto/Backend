const { query } = require('express');
const connection = require('../db/connection');

const users = {
    getInvitesById: 
    (id) => new Promise((resolve, reject) => {
        connection.query(`SELECT invite_data.id, sender_id, sending_date, is_accepted, username, email, first_name, role
        FROM sent_invites
        LEFT JOIN invite_data ON invite_data.id = invite_id
        LEFT JOIN Users ON Users.id = target_id
        WHERE invite_id = ${id};`, 
        (err, result) => {
        if (err) {
            reject(err);
        }
            resolve(result);
        });
    }),
    postInvite: 
    (body) => new Promise((resolve, reject) => {
        const values = [body.sender_id, body.form_id, body.email];
        const sqlQuery = `
    START TRANSACTION;
    INSERT INTO invite_data (sender_id, form_id) 
    VALUES (?, ?);
    INSERT INTO sent_invites (target_id, invite_id) 
    VALUES ((SELECT id FROM Users WHERE email = ?), LAST_INSERT_ID());
    COMMIT;
`;
        connection.query(sqlQuery,values, 
        (err, result) => {
        if (err) {
            reject(err);
        }
            resolve(result);
        });
    })
}


module.exports = users;