const connection = require('../db/connection');


const forms = {
    getUserForms:
    (id) => new Promise((resolve, reject) => {
        connection.query(`SELECT * FROM Forms WHERE id = ${id}`, (err, result) => {
            if (err) {
                reject(err);
            }
                resolve(result);
        });
    }),
    postForm:
    (form) => new Promise((resolve, reject) => {
        connection.query(`INSERT INTO Forms (form_name, form_location, form_classification, form_type) 
        VALUES ("${form.form_name}","${form.form_location}","${form.form_classification}","${form.form_type}")`,(err, result) => {
            if (err) {
                reject(err);
            }
                resolve(result);
        });
    })
}

module.exports = forms