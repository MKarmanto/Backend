const fs = require('fs').promises;
const bcrypt = require('bcrypt');

let users = [];

async function initialize() {
  try {
    const data = await fs.readFile('users.json', 'utf8');
    users = JSON.parse(data);
  } catch (err) {
    console.error('Error reading users.json:', err);
  }
}

async function findUser(username, password) {
  const user = users.find(user => user.username === username);
  if (user && await bcrypt.compare(password, user.password)) {
    return user;
  }
  return null;
}

module.exports = {
  initialize,
  findUser
};