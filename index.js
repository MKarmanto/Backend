//to run: 
//npm install
//nodemon index.js

const express = require('express');
const userRouter = require('./routes/Users');
const templateRouter = require('./routes/Templates');
const formRouter = require('./routes/Forms')
const inviteRouter = require('./routes/Invite_data');
const cors = require('cors');
const bodyParser = require('body-parser');


const app = express();
app.use(cors())
app.use(bodyParser.json())
app.use(cors({ origin: 'http://localhost:3001' }), userRouter);
app.use(cors({ origin: 'http://localhost:3001' }), templateRouter);
app.use(cors({ origin: 'http://localhost:3001' }), formRouter);
app.use(cors({ origin: 'http://localhost:3001' }), inviteRouter);
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.json);


app.listen(3000, () => { // Use the correct port for your backend
    console.log('Backend server is running on port 3000');
});
