const express = require('express');

const {
    getUserForms,
    postForm
} = require('../controller/Forms');

const router = express.Router({mergeParams: true});

router.get('/userFormsById/:id', getUserForms);
router.post('/postForm', postForm);


module.exports = router;