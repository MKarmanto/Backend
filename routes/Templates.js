const express = require('express');

const {
    getTemplates,
    getAll,
    getQuestionById,
    getTemplateById,
    getQuestionByTemplateId,
    updateTemplate,
    addTemplateQuestions
} = require('../controller/Templates');

const router = express.Router({mergeParams: true});

router.get('/usertemplates', getTemplates);
router.get('/allTemplates', getAll);
router.get('/templateById/:id', getTemplateById);
router.get('/questionsById/:id', getQuestionById);
router.get('/questionsByTemplateId/:id', getQuestionByTemplateId)
router.put('/updateTemplate', updateTemplate)
router.post('/addTemplateQuestions', addTemplateQuestions)

module.exports = router;