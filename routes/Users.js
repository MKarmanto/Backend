
const express = require('express');

const {
    postLogin,
} = require('../controller/Users');

const router = express.Router({mergeParams: true});

router.post('/login', postLogin);

module.exports = router;