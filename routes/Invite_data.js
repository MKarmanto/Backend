const express = require('express');

const {
    invitesById, 
    postInvite
} = require('../controller/Invites');

const router = express.Router({mergeParams: true});

router.get('/getInvitesById/:id', invitesById);
router.post('/postInvite', postInvite)

module.exports = router;