const templates = require('../model/Templates');

const getTemplates = async (req, res) => {
    const { username} = req.body;
    try {
        const response = await templates.getTemplates(username);
        if(response){
            res.send(response);
        } else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}

const getAll = async (req, res) => {
    try {
        const response = await templates.getAll();
        if(response){
            res.send(response);
        } else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}

const getQuestionById = async (req, res) => {
    try {
        const id = parseInt(req.params.id, 10)
        const response = await templates.getQuestionsById(id);
        if(response){
            res.send(response)
        }else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}

const getTemplateById = async (req, res) => {
    try {
        const id = parseInt(req.params.id, 10)
        const response = await templates.getTemplateById(id);
        if(response){
            res.send(response)
        }else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}
const getQuestionByTemplateId = async (req, res) => {
    try {
        const id = parseInt(req.params.id, 10)
        const response = await templates.getQuestionsByTemplateId(id);
        if(response){
            res.send(response)
        }else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}

const updateTemplate = async (req, res) => {
    try {
        const template = {
            id: (req.body.id !== undefined ? req.body.id : null),
            name: (req.body.name !== undefined ? `'${req.body.name}'` : null),
            location: (req.body.location !== undefined ? `'${req.body.location}'` : null),
            classification: (req.body.classification !== undefined ? `'${req.body.classification}'` : null),
            type: (req.body.type !== undefined ? `'${req.body.type}'` : null)
        }
        const response = await templates.updateTemplate(template);
        if(response){
            res.send(response)
        }else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}

const addTemplateQuestions = async (req, res) =>{
    try {
        const template_id = req.body.id
        const questionsArray = req.body.questions
        const response = await templates.addTemplateQuestions(template_id, questionsArray);
        if(response){
            res.status(200).json({message: "post request was successful"})
        }else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    getTemplates,
    getAll,
    getQuestionById,
    getTemplateById,
    getQuestionByTemplateId,
    updateTemplate,
    addTemplateQuestions
}