const userModel = require('../model/userModel');

async function login(req, res) {
  const { username, password } = req.body;
  const user = await userModel.findUser(username, password);
  
  if (user) {
    res.status(200).json({ message: 'Login successful', role: user.role });
  } else {
    res.status(401).json({ message: 'Invalid credentials' });
  }
}

module.exports = {
  login
};