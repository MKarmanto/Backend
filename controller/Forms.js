const forms = require('../model/Forms');

const getUserForms = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await forms.getUserForms(id);
        if(response){
            res.send(response);
        } else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}

const postForm = async (req, res) => {
    const form = {
        form_name: req.body.name,
        form_location: req.body.location,
        form_classification: req.body.classification,
        form_type: req.body.type
    }
    try {
        const response = await forms.postForm(form);
        if(response){
            res.status(200).json({message: "succesful POST request"});
        } else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}


module.exports = {
    getUserForms,
    postForm
}