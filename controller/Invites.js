const users = require('../model/Invites');

const invitesById = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await users.getInvitesById(id);
        if(response.length != 0){
            res.send(response);
        } else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}
const postInvite = async (req, res) => {
    const body = {
        form_id: req.body.form_id,
        sender_id: req.body.sender_id,
        email: req.body.email
    }
    try {
        const response = await users.postInvite(body);
        if(response.length != 0){
            res.send(response);
        } else {
            res.status(404).json({message: "database response not found"});
        }
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    invitesById,
    postInvite
}