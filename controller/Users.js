const users = require('../model/Users');

const postLogin = async (req, res) => {
    const { username, password } = req.body;
    try {
        const response = await users.findLogin(username, password);
        if(response.length>1) {
            res.status(404).json({ message: 'database query problem'})
        }
        if(response.length != 0){
            res.status(200).json({message: 'Login successful', role: response[0].role, firstName: response[0].first_name});
        } else {
            res.status(401).json({ message: 'Invalid credentials' });
        }
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    postLogin
}